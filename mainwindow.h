#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAbstractTableModel>
#include "Booking/TravelAgency.h"
#include "tablebookingsmodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void updateData();
public slots:
    void open_file();
    void onTableClicked(const QModelIndex &index);
    void newBooking();
    void save();
    void openChecking();

private:
    Ui::MainWindow *ui;
    TravelAgency *travelAgency;

    TableBookingsModel *tableBookingModel;
    void parseData();

    Booking *currentBooking;
};

#endif // MAINWINDOW_H
