#include "mainwindow.h"
#include "newbookingwindow.h"
#include "ui_newbookingwindow.h"

enum BookingType {
    FlightBooking           = 0,
    HotelBooking            = 1,
    RentalCarReservation    = 2,
};

NewBookingWindow::NewBookingWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::NewBookingWindow)
{
    ui->setupUi(this);

    ui->buchungTypeLabel->addItem("Flugbuchung",FlightBooking);
    ui->buchungTypeLabel->addItem("HotelBooking",HotelBooking);
    ui->buchungTypeLabel->addItem("CarBooking",RentalCarReservation);

    connect(ui->buchungTypeLabel, QOverload<int>::of(&QComboBox::activated),
            [=](int index){
        if (index == FlightBooking) {
            this->ui->flightBox->show();
        } else {
            this->ui->flightBox->hide();
        }
        if (index == HotelBooking) {
            this->ui->hotelBox->show();
        } else {
            this->ui->hotelBox->hide();
        }
        if (index == RentalCarReservation) {
            this->ui->carBox->show();
        } else {
            this->ui->carBox->hide();
        }
    });

    this->ui->hotelBox->hide();
    this->ui->carBox->hide();

}

NewBookingWindow::~NewBookingWindow()
{
    delete ui;
}


void NewBookingWindow::on_pushButton_clicked()
{
    string fromDate = ui->startDatumLabel->date().toString("yyyyMMdd").toUtf8().constData();
    string toDate = ui->enddatumLabel->date().toString("yyyyMMdd").toUtf8().constData();

    switch (ui->buchungTypeLabel->currentIndex()) {
    case FlightBooking: {
        travelAgency->creatingBooking('F',
                                      ui->preisLabel->value(),
                                      fromDate,
                                      toDate,
                                      ui->reiseIDLabel->value(),
        {ui->startDatumLabel->text().toUtf8().constData(),
         ui->Zielflughafen->text().toUtf8().constData(),
         ui->Fluglinie->text().toUtf8().constData(),
         ui->seatPrefTextField->text().toUtf8().constData()
                                      });
        break;
    }
    case HotelBooking: {
        travelAgency->creatingBooking('H',
                                      ui->preisLabel->value(),
                                      fromDate,
                                      toDate,
                                      ui->reiseIDLabel->value(),
        {ui->hotenName->text().toUtf8().constData(),
         ui->cityName->text().toUtf8().constData(),
         ui->isSmoke->checkState() == Qt::Checked ? "1" : "0"
                                      });
        break;
    }
    case RentalCarReservation: {
        travelAgency->creatingBooking('R',
                                      ui->preisLabel->value(),
                                      fromDate,
                                      toDate,
                                      ui->reiseIDLabel->value(),
        {ui->pickupLocation->text().toUtf8().constData(),
         ui->returnLocation->text().toUtf8().constData(),
         ui->company->text().toUtf8().constData(),
         ui->insuranceTypeTextField->text().toUtf8().constData()
                                      });
        break;
    }
    }

    if (parent() != nullptr) {
        MainWindow *mainWindow = dynamic_cast<MainWindow*>(parent());
        if (mainWindow != nullptr) {
            qInfo("Update");
            mainWindow->updateData();
        }
    }

    close();
}
