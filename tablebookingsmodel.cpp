#include "tablebookingsmodel.h"
#include <vector>
#include "Travel/Travel.hpp"

TableBookingsModel::TableBookingsModel(TravelAgency *travelAgency) : travelAgency(travelAgency)
{

}

QVariant TableBookingsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole && orientation != Qt::Vertical)
        return QVariant();
    if (section == 0) {
        return QVariant("Buchungnummer");
    } else if (section == 1) {
        return QVariant("Preis");
    } else if (section == 2) {
        return QVariant("Kunde");
    } else if (section == 3) {
        return QVariant("Reisnummer");
    }
    return  QVariant();
}


Qt::ItemFlags QAbstractTableModel::flags( const QModelIndex& index ) const
{
    return Qt::ItemIsEnabled;
}


int TableBookingsModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return travelAgency->allBookings.size();
}

int TableBookingsModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 4;
}

QVariant TableBookingsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    Booking *booking = travelAgency->allBookings[index.row()];
    Travel *travel = travelAgency->findTravel(booking->getTravel_id());
    Customer *customer = travelAgency->findCustomer(travel->getCustomerId());

    if (index.column() == 0) {
        return QVariant((int)booking->getId());
    } else if (index.column() == 1) {
        return QVariant(booking->getPrice());
    } else if (index.column() == 2) {
        return QVariant(QString::fromStdString(customer->getName()));
    } else if (index.column() == 3) {
        return QVariant((int)travel->getId());
    }
    return QVariant();
}
