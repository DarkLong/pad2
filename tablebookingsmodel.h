#ifndef TABLEBOOKINGSMODEL_H
#define TABLEBOOKINGSMODEL_H

#include <QAbstractTableModel>
#include <Booking/TravelAgency.h>

class TableBookingsModel : public QAbstractTableModel
{
    Q_OBJECT

public:
     TableBookingsModel(TravelAgency *travelAgency);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    TravelAgency *travelAgency;
};

#endif // TABLEBOOKINGSMODEL_H
