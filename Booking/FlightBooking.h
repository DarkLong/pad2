#ifndef FlightBooking_hpp
#define FlightBooking_hpp

#include <string>
#include "libr/Components.h"
#include "Booking/Booking.h"

using namespace std;

class FlightBooking : public Booking
{
private:
    string toDest;
    string fromDest;
    string airline;
    char seatPref;
public:
    FlightBooking(Components components);
    FlightBooking(ifstream &file);
    ~FlightBooking();
    double getPrice();
    string showDetail();
    string parseToString(long customerID,string name);
    string getFromDest() const;
    string getAirline() const;
    string getToDest() const;
    char getSeatPref() const;
};

#endif
