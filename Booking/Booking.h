#ifndef Booking_hpp
#define Booking_hpp

#include <iostream>
#include <string>

using namespace std;

class Booking
{
    // static method id.
protected:
    long id;
    long travel_id;
    double price;
    string fromDate;
    string toDate;
public:
    double getPrice();
    long getId();
    virtual string showDetail() = 0;
    virtual string parseToString(long customerID,string name);
    long getTravel_id() const;
    string getFromDate() const;
    string getToDate() const;

    bool operator<(const Booking &c1) {
        return this->getTravel_id() < c1.getTravel_id();
    }

    bool operator>(const Booking &c1) {
        return this->getTravel_id() >= c1.getTravel_id();
    }

    virtual ~Booking();
};

class DataCorruptionException : public exception {
public:
    string msg;

    DataCorruptionException(string msg) {
        this->msg = msg;
    };

    virtual const char* what() const _NOEXCEPT {
        return msg.c_str();
    }
};

#endif
