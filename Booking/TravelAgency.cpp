#include <iostream>
#include <fstream>
#include "Booking/TravelAgency.h"
#include "libr/Components.h"

#include "FlightBooking.h"
#include "RentalCarReservation.h"
#include "HotelBooking.h"

using namespace std;

bool bookingCompare(Booking* const &left,Booking* const &right) {
    return left->getTravel_id() > right->getTravel_id();
}

TravelAgency::TravelAgency(/* args */) : allBookings(&bookingCompare)
{

}

TravelAgency::~TravelAgency()
{
}

void TravelAgency::reset() {
    allTravel.reset();
    Travel* travel;
    while (!allTravel.isAtEnd()) {
        travel = allTravel.deleteNode();
    }

    allBookings.reset();
    Booking* booking;
    while (!allBookings.isAtEnd()) {
        booking = allBookings.deleteNode();
    }

    allCustomer.reset();
    Customer* customer;
    while (!allCustomer.isAtEnd()) {
         customer = allCustomer.deleteNode();
    }
}

bool TravelAgency::readFile(string filePath) {
    ifstream file;
    file.open(filePath, ios::in);
    if (!file) {
        cerr << "File ist not found";
        return false;
    }
    char line[256];
    while (!file.eof()) {
        file.getline(line, 256);
        Components components = parse(line);
        Booking *booking;
        if (components[0] == "F") {
            booking = new FlightBooking(components);
        } else if (components[0] == "H") {
            booking = new HotelBooking(components);
        } else if (components[0] == "R") {
            booking = new RentalCarReservation(components);
        }
        allBookings.insertNode(booking);

        long idTravel = booking->getTravel_id();
        long idCustomer;

        try {
           idCustomer= stol(components[6]);
        } catch (invalid_argument e) {
            string str = "HotelBooking Error: " + string(e.what()) + ". Line: " + to_string(components);
            throw DataCorruptionException(str);
        } catch (out_of_range e) {
            string str = "HotelBooking Error: " + string(e.what()) + ". Line: " + to_string(components);
            throw DataCorruptionException(str);
        }

        Customer *customer = findCustomer(idCustomer);
        if (customer == nullptr) {
            customer = new Customer(components);
            allCustomer.insertNode(customer);
        }
        
        Travel *travel = findTravel(idTravel);
        if (travel == nullptr) {
            travel = new Travel(components);
            allTravel.insertNode(travel);
        }

        travel->relationship.insertVertex(booking->getId(),booking);

        travel->addBooking(booking);
        customer->addTravel(travel);
    }



    file.clear();                 // clear fail and eof bits
    file.seekg(0, std::ios::beg);

    while (!file.eof()) {
        file.getline(line, 256);
        Components components = parse(line);

        Booking *booking = findBooking(stol(components[1]));
        Travel *travel = findTravel(booking->getTravel_id());
        if (components[0] == "F") {
            if (components.size() > 12) {
                travel->relationship.insertArc(stol(components[12]),booking->getId());
            }
            if (components.size() > 13) {
                travel->relationship.insertArc(stol(components[13]),booking->getId());
            }
        } else if (components[0] == "H") {
            if (components.size() > 11) {
                travel->relationship.insertArc(stol(components[11]),booking->getId());
            }
            if (components.size() > 12) {
                travel->relationship.insertArc(stol(components[12]),booking->getId());
            }
        } else if (components[0] == "R") {
            if (components.size() > 12) {
                travel->relationship.insertArc(stol(components[12]),booking->getId());
            }
            if (components.size() > 13) {
                travel->relationship.insertArc(stol(components[13]),booking->getId());
            }
        }


    }

    file.close();
    return 1;
}

void TravelAgency::print() {
    int flightCount = 0;
    int rentalCount = 0;
    int hotelsCount = 0;
    
    double totalPrice = 0;
    
    for (int x = 0; x < allBookings.size(); x++) {
        totalPrice += allBookings[x]->getPrice();
        if(dynamic_cast<FlightBooking*>(allBookings[x])) {
            flightCount++;
        } else if(dynamic_cast<RentalCarReservation*>(allBookings[x])) {
            rentalCount++;
        } else if(dynamic_cast<HotelBooking*>(allBookings[x])) {
            hotelsCount++;
        }
    }

    cout << "Es wurden " << flightCount << " Flugreservierung, " << hotelsCount << " Hotelbuchungen, " << rentalCount << " Mietwagenreservierung im Gesamstwert von " << totalPrice << " eingelesen. Es wurden " << allTravel.size() << " Reisen und " << allCustomer.size() << " Kunden angelegt. Der Kunde mit ID 1 hat " << findCustomer(1)->travelCount() << " Reisen gebucht. Zur Reise mit der ID 17 gehoren " << findTravel(17)->bookingCount() << " Buchungen" << endl;
}

Booking* TravelAgency::findBooking(long id) {
    for (int x = 0; x < allBookings.size(); x++) {
        if (allBookings[x]->getId() == id) {
            return allBookings[x];
        }
    }
    return nullptr;
}
Travel* TravelAgency::findTravel(long id) {
    for (int x = 0; x < allTravel.size(); x++) {
        if (allTravel[x]->getId() == id) {
            return allTravel[x];
        }
    }
    return nullptr;
}
Customer* TravelAgency::findCustomer(long id) {
    for (int x = 0; x < allCustomer.size(); x++) {
        if (allCustomer[x]->getId() == id) {
            return allCustomer[x];
        }
    }
    return nullptr;
}

int TravelAgency::creatingBooking(char type, double price, string start, string end, long travelID, Components bookingsDetail) {
    Travel *travel = findTravel(travelID);
    if (travel == nullptr) {
        return -1;
    }

    Customer *customer = findCustomer(travel->getCustomerId());

    int nextID = 0;
    for (int x = 0; x < allBookings.size(); x++) {
        if (allBookings[x]->getId() > nextID) {
            nextID = allBookings[x]->getId();
        }
    }
    nextID++;
    
    Booking *b;
    if (type == 'F') {
        b = new FlightBooking({"",to_string(nextID),to_string(price),start,end,to_string(travel->getId()),to_string(travel->getCustomerId()),"",bookingsDetail[0],bookingsDetail[1],bookingsDetail[2],bookingsDetail[3]});
        
    } else if (type == 'H') {
        b = new HotelBooking({"",to_string(nextID),to_string(price),start,end,to_string(travel->getId()),to_string(travel->getCustomerId()),"",bookingsDetail[0],bookingsDetail[1],bookingsDetail[2]});

    } else if (type == 'R') {
        b = new RentalCarReservation({"",to_string(nextID),to_string(price),start,end,to_string(travel->getId()),to_string(travel->getCustomerId()),"",bookingsDetail[0],bookingsDetail[1],bookingsDetail[2],bookingsDetail[3]});
    
    }
    allBookings.insertNode(b);
    travel->addBooking(b);
    customer->addTravel(travel);

    cout << allBookings.size();
    return nextID;
}

void TravelAgency::printCheckTravel() {
    for (int x = 0; x < allTravel.size(); x++) {
        Travel *travel = allTravel[x];
        cout << travel->getId() << " " << travel->checkMissingHotel() << " " << travel->checkNeedlessHotel() << " " << travel->checkNeedlessRentalCar() << " " << travel->checkRoundTrip() << endl;
    }
}

void TravelAgency::save(string filePath) {
    ofstream file;
    file.open(filePath);

    if (!file) {
        throw FileNotFoundException();
    }

    for (int x = 0; x < allBookings.size(); x++) {
        Booking *booking = allBookings[x];
        Travel *travel = findTravel(booking->getTravel_id());
        Customer *customer = findCustomer(travel->getCustomerId());

        file << booking->parseToString(customer->getId(),customer->getName());
        if (x < allBookings.size()-1) file << "\n";
    }

    file.close();
}
