#ifndef HotelBooking_hpp
#define HotelBooking_hpp

#include <string>
#include "libr/Components.h"
#include "Booking/Booking.h"

using namespace std;

class HotelBooking : public Booking
{
private:
    string hotel;
    string town;
    bool smoke;
public:
    HotelBooking(Components components);
    HotelBooking(ifstream &file);
    ~HotelBooking();
    double getPrice();
    string showDetail();
    string parseToString(long customerID,string name);
    string getHotel() const;
    string getTown() const;
    bool getSmoke() const;
};

#endif

