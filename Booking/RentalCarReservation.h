#ifndef RentalCarReservation_hpp
#define RentalCarReservation_hpp

#include <stdio.h>
#include <string>
#include "libr/Components.h"
#include "Booking/Booking.h"

using namespace std;

class RentalCarReservation : public Booking
{
private:
    string pickupLocation;
    string returnLocation;
    string company;
    string insuranceType;
public:
    RentalCarReservation(Components components);
    RentalCarReservation(ifstream &file);
    ~RentalCarReservation();
    double getPrice();
    string showDetail();
    string parseToString(long customerID,string name);
    string getPickupLocation() const;
    string getReturnLocation() const;
    string getCompany() const;
    string getInsuranceType() const;
};

#endif

