#include "HotelBooking.h"
#include <fstream>
#include <iostream>

string HotelBooking::getHotel() const
{
    return hotel;
}

string HotelBooking::getTown() const
{
    return town;
}

bool HotelBooking::getSmoke() const
{
    return smoke;
}

HotelBooking::HotelBooking(Components components)
{
    try {
       id         = stol(components[1]);
       price      = stod(components[2]);
       fromDate   = components[3];
       toDate     = components[4];
       travel_id  = stol(components[5]);
       hotel      = components[8];
        town       = components[9];
       if (components[10] == "1") {
          smoke = true;
      } else {
          smoke = false;
       }
    } catch (invalid_argument e) {
        string str = "HotelBooking Error: " + string(e.what()) + ". Line: " + to_string(components);
        throw DataCorruptionException(str);
    } catch (out_of_range e) {
        string str = "HotelBooking Error: " + string(e.what()) + ". Line: " + to_string(components);
        throw DataCorruptionException(str);
    }
}

HotelBooking::~HotelBooking()
{
    Booking::~Booking();
}

string HotelBooking::showDetail() {
    return smoke == true ? "1" : "0";
}

string HotelBooking::parseToString(long customerID,string name) {
    return   "H|"
            +to_string(id)
            + "|"
            +to_string(price)
            + "|"
            + fromDate
            + "|"
            + toDate
            + "|"
            + to_string(travel_id)
            + "|"
            + to_string(customerID)
            + "|"
            + name
            + "|"
            + hotel
            + "|"
            + town
            + "|"
            + (smoke ? "1" : "0");
}
