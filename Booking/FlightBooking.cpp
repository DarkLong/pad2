#include "FlightBooking.h"
#include <fstream>
#include <iostream>

using namespace std;

string FlightBooking::getFromDest() const
{
    return fromDest;
}

string FlightBooking::getAirline() const
{
    return airline;
}

string FlightBooking::getToDest() const
{
    return toDest;
}

char FlightBooking::getSeatPref() const
{
    return seatPref;
}

FlightBooking::FlightBooking(Components components)
{
    try {
        id          = stol(components[1]);
        price       = stod(components[2]);
        fromDate    = components[3];
        toDate      = components[4];
        travel_id   = stol(components[5]);
        fromDest    = components[8];
        toDest      = components[9];
        airline     = components[10];
        seatPref    = components[11][0];
    } catch (invalid_argument e) {
        string str = "FlightBooking Error: " + string(e.what()) + ". Line: " + to_string(components);
        throw DataCorruptionException(str);
    } catch (out_of_range e) {
        string str = "FlightBooking Error: " + string(e.what()) + ". Line: " + to_string(components);
        throw DataCorruptionException(str);
    }
}

FlightBooking::~FlightBooking()
{
    Booking::~Booking();
}


string FlightBooking::showDetail(){
    string str = "";
    str += seatPref;
    return str;
}

string FlightBooking::parseToString(long customerID,string name) {
    return   "F|"
            +to_string(id)
            + "|"
            +to_string(price)
            + "|"
            + fromDate
            + "|"
            + toDate
            + "|"
            + to_string(travel_id)
            + "|"
            + to_string(customerID)
            + "|"
            + name
            + "|"
            + fromDest
            + "|"
            + toDest
            + "|"
            + airline
            + "|"
            + seatPref;
}
