//
//  Booking.cpp
//  Praktikum 1 Xcode
//
//  Created by Tran Giang Long on 13.04.19.
//  Copyright © 2019 Tran Giang Long. All rights reserved.
//

#include "Booking.h"

string Booking::showDetail() {
    return "Nothing here";
}

string Booking::parseToString(long customerID,string name) {
    return "Nothing here";
}

long Booking::getId() {
    return id;
}

long Booking::getTravel_id() const
{
    return travel_id;
}

string Booking::getFromDate() const
{
    return fromDate;
}

string Booking::getToDate() const
{
    return toDate;
}

double Booking::getPrice() {
    return price;
}

Booking::~Booking() {

}
