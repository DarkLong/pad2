#include "RentalCarReservation.h"
#include <fstream>
#include <iostream>

using namespace std;

string RentalCarReservation::getPickupLocation() const
{
    return pickupLocation;
}

string RentalCarReservation::getReturnLocation() const
{
    return returnLocation;
}

string RentalCarReservation::getCompany() const
{
    return company;
}

string RentalCarReservation::getInsuranceType() const
{
    return insuranceType;
}

RentalCarReservation::RentalCarReservation(Components components)
{
    try {
        id              = stol(components[1]);
        price           = stod(components[2]);
        fromDate        = components[3];
        toDate          = components[4];
        travel_id   = stol(components[5]);
        pickupLocation  = components[8];
        returnLocation  = components[9];
        company         = components[10];
        insuranceType   = components[11];
    } catch (invalid_argument e) {
        string str = "RentalCarReservation init error: " + string(e.what()) + ". Line: " + to_string(components);
        throw DataCorruptionException(str);
    } catch (out_of_range e) {
        string str = "RentalCarReservation Error: " + string(e.what()) + ". Line: " + to_string(components);
        throw DataCorruptionException(str);
    }

}

RentalCarReservation::~RentalCarReservation()
{
    Booking::~Booking();
}

string RentalCarReservation::showDetail() {
    return insuranceType;
}

string RentalCarReservation::parseToString(long customerID,string name) {
    return   "R|"
            +to_string(id)
            + "|"
            +to_string(price)
            + "|"
            + fromDate
            + "|"
            + toDate
            + "|"
            + to_string(travel_id)
            + "|"
            + to_string(customerID)
            + "|"
            + name
            + "|"
            + pickupLocation
            + "|"
            + returnLocation
            + "|"
            + company
            + "|"
            + insuranceType;
}

