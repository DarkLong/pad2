#ifndef TravelAgency_hpp
#define TravelAgency_hpp

#include "libr/sortedlinkedlist.h"
#include "libr/graph.h"
#include "Booking/Booking.h"
#include "Travel/Travel.hpp"
#include "Travel/Customer.hpp"

using namespace std;

class TravelAgency
{

public:
    TravelAgency(/* args */);
    ~TravelAgency();
    bool readFile(string filePath);
    void reset();
    void print();
    void save(string filePath);
    
    Booking* findBooking(long id);
    Travel* findTravel(long id);
    Customer* findCustomer(long id);
    
    int creatingBooking(char type, double price, string start, string end, long travelID, Components bookingsDetail);

    SortedLinkedList<Booking*> allBookings;
    LinkedList<Travel*> allTravel;
    LinkedList<Customer*> allCustomer;

    void printCheckTravel();
};

class FileNotFoundException : public exception {
public:
    string msg;
    virtual const char* what() const _NOEXCEPT {
        return "File is not found";
    }
};

#endif
