#include <vector>
using namespace std;

typedef vector<string> Components;
Components parse(string source);
string to_string(Components component);
