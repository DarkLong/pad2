#ifndef NODE_H
#define NODE_H

template <class T>
class Node
{
public:
    Node() {
        this->prev = nullptr;
        this->next = nullptr;
    }

    Node(T data)
    {
        this->data = data;
        this->prev = nullptr;
        this->next = nullptr;
    }

    T getData() const
    {
        return data;
    }
    void setData(const T &value)
    {
        data = value;
    }

    Node<T> *getNext() const
    {
        return next;
    }
    void setNext(Node<T> *value)
    {
        next = value;
    }

    Node<T> *getPrev() const
    {
        return prev;
    }
    void setPrev(Node<T> *value)
    {
        prev = value;
    }

private:
    T data;
    Node<T> *next;
    Node<T> *prev;
};

#endif // NODE_H
