

#ifndef LIST_H
#define LIST_H

#include "node.h"
#include <stdexcept>
#include <string>

template <class T>
class LinkedList
{
public:
    LinkedList()
    {
        root = nullptr;
        end = new Node<T>();
        cursor = end;

    }

    T operator[] (int index) {
        Node<T> *node = root;
        while (index > 0 && node != end)
        {
            node = node->getNext();
            index-= 1;
        }
        if (index > 0) {
            throw std::out_of_range("Out Range: " + std::to_string(index));
        }
        return node->getData();
    }

    long int size() {
        return mSize;
    }

    void insertNode(T data)
    {
        Node<T> *node = new Node<T>(data);

        // Empty List
        if (root == nullptr)
        {
            root = node;
            node->setNext(end);
            end->setPrev(node);
        }
        else
        {
            node->setNext(cursor);
            node->setPrev(cursor->getPrev());
            
            if (cursor->getPrev())
            {
                cursor->getPrev()->setNext(node);
            }
            cursor->setPrev(node);
        }
        cursor = node;
        mSize += 1;
        if (!cursor->getPrev()) root = cursor;
    }

    bool isAtEnd()
    {
        return cursor == end || cursor == nullptr;
    }

    void advance()
    {
        if (cursor && cursor->getNext()) {
            cursor = cursor->getNext();
        }   
    }

    T deleteNode()
    {
        Node<T> *tmpPrev;
        Node<T> *tmpNext;

        if (root == nullptr)
        {
            throw std::underflow_error("No Element to delete");
        }
        else
        {
            if (cursor->getPrev() == nullptr)
            {
                if (cursor->getNext() == end)
                {
                    T value = cursor->getData();
                    delete cursor;
                    root = nullptr;
                    end->setPrev(nullptr);
                    cursor = end;
                    mSize-=1;
                    return value;
                }
                else
                {
                    cursor = cursor->getNext();
                    T value = root->getData();
                    delete root;
                    cursor->setPrev(nullptr);
                    root = cursor;
                    mSize-=1;
                    return value;
                }
            }
            else
            {
                if (cursor->getNext() == nullptr)
                {
                    tmpPrev = cursor->getPrev();
                    T value = cursor->getData();
                    delete cursor;
                    cursor = end;
                    end->setPrev(tmpPrev);
                    tmpPrev->setNext(end);
                    mSize-=1;
                    return value;
                }
                else
                {
                    tmpPrev = cursor->getPrev();
                    tmpNext = cursor->getNext();

                    T value = cursor->getData();
                    delete cursor;

                    tmpPrev->setNext(tmpNext);
                    tmpNext->setPrev(tmpPrev);
                    cursor = tmpPrev;
                    mSize-=1;
                    return value;
                }
            }
        }


    }
    T getNode()
    {
        return cursor->getData();
    }
    void reset()
    {
        cursor = root;
    }

protected:
    Node<T> *root;
    Node<T> *cursor;
    Node<T> *end;

    long int mSize = 0;

    bool iteratorMode;
    Node<T> *iteratorNode;
};

#endif // LIST_H
