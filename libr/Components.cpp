#include "Components.h"
#include <iostream>
using namespace std;

Components parse(string source) {
    int x = 0;
    int y = 1;
    Components components;
    for (; y < source.length(); y++) {
        if (source[y] == '|') {
            components.push_back(source.substr(x,y-x));
            x = y+1;
        }
    }
    components.push_back(source.substr(x,y-x));
    return components;
}

string to_string(Components component) {
    string str = "";
    for (int x = 0; x < component.size(); x++) {
        str += component[x] + " | ";
    }
    return str;
}
