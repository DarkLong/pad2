#ifndef SORTEDLINKEDLIST_H
#define SORTEDLINKEDLIST_H

#include "list.h"
#include "node.h"

template <class T>

class SortedLinkedList : public LinkedList<T> {

public:
    typedef bool (*SortedLinkedListCompare)(const T &leftValue,const T &rightValue);

    SortedLinkedList(SortedLinkedListCompare compare) : LinkedList<T>() {
        this->compare = compare;
    }

    void insertNode(T data) {

        if (this->root == nullptr) {
            LinkedList<T>::insertNode(data);
            return;
        }

        this->cursor = this->root;
        while ( compare(data, this->cursor->getData()) )  {
            this->cursor = this->cursor->getNext();
            if (this->cursor == this->end) break;
        }

        LinkedList<T>::insertNode(data);
        return;
    }

private:
    SortedLinkedListCompare compare;
};

#endif // SORTEDLINKEDLIST_H
