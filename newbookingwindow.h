#ifndef NEWBOOKINGWINDOW_H
#define NEWBOOKINGWINDOW_H

#include <QMainWindow>

#include <Booking/TravelAgency.h>

namespace Ui {
class NewBookingWindow;
}

class NewBookingWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit NewBookingWindow(QWidget *parent = nullptr);
    ~NewBookingWindow();
    TravelAgency *travelAgency;
private slots:
    void on_pushButton_clicked();

private:
    Ui::NewBookingWindow *ui;
};

#endif // NEWBOOKINGWINDOW_H
