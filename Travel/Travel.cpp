//
//  Travel.cpp
//  Praktikum 1 Xcode
//
//  Created by Tran Giang Long on 13.04.19.
//  Copyright © 2019 Tran Giang Long. All rights reserved.
//

#include "Travel.hpp"
#include "libr/algorithmen.cpp"

#include <libr/graph.h>
#include <libr/heap.h>

#include <Booking/FlightBooking.h>
#include <Booking/HotelBooking.h>
#include <Booking/RentalCarReservation.h>

long Travel::getCustomerId() const
{
    return customerId;
}

void Travel::setCustomerId(long value)
{
    customerId = value;
}

static vector<Booking*> tmpBookings;

static FlightBooking* getLastFlight() {

    for (int x = 0; x < tmpBookings.size(); x++) {
        if (FlightBooking* booking = dynamic_cast<FlightBooking*>(tmpBookings[x]))
            return booking;
    }
    return nullptr;
}

static FlightBooking* getFirstFlight() {

    for (int x = tmpBookings.size()-1; x >= 0; x--) {
        if (FlightBooking* booking = dynamic_cast<FlightBooking*>(tmpBookings[x]))
            return booking;
    }
    return nullptr;
}

static void visit(Graph<Booking*, 150> &g, int n) {
    tmpBookings.push_back(g.getVertexValue(n));
}

bool Travel::checkRoundTrip()
{
    tmpBookings.clear();
    DepthFirstSearch(this->relationship, &visit);

    FlightBooking *startFlight = getFirstFlight();
    FlightBooking *endFlight   = getLastFlight();

    if (startFlight->getFromDest() == endFlight->getToDest())
        return true;

    return false;
}

bool Travel::checkMissingHotel()
{
    tmpBookings.clear();
    DepthFirstSearch(this->relationship, &visit);

    Booking *prev = nullptr;
    while (!tmpBookings.empty()) {
        Booking *cur = tmpBookings.back();
        tmpBookings.pop_back();

        if (dynamic_cast<RentalCarReservation*>(cur)) {
            continue;
        }

        if (prev != nullptr) {
            if (prev->getToDate() < cur->getFromDate()) {
                return false;
            }
        }
        prev = cur;
    }

    return true;
}

bool Travel::checkNeedlessHotel()
{
    tmpBookings.clear();
    DepthFirstSearch(this->relationship, &visit);

    Booking *prev = nullptr;
    while (!tmpBookings.empty()) {
        Booking *cur = tmpBookings.back();
        tmpBookings.pop_back();

        if (dynamic_cast<RentalCarReservation*>(cur)) {
            continue;
        }

        if (prev != nullptr) {
            if (prev->getToDate() > cur->getFromDate()) {
                return false;
            }
        }
        prev = cur;
    }
    return true;
}

bool Travel::checkNeedlessRentalCar() {
    tmpBookings.clear();
    DepthFirstSearch(this->relationship, &visit);

    Booking *prev = nullptr;
    while (!tmpBookings.empty()) {
        Booking *cur = tmpBookings.back();
        tmpBookings.pop_back();

        if (dynamic_cast<HotelBooking*>(cur)) {
            continue;
        }

        if (prev != nullptr) {
            if (prev->getToDate() > cur->getFromDate()) {
                return false;
            }
        }
        prev = cur;
    }
    return true;
}

Travel::Travel(long id, long customerId) {
    this->id = id;
    this->customerId = customerId;
}

Travel::Travel(Components components) {
    id = stol(components[5]);
    customerId = stol(components[6]);
}

long Travel::getId() {
    return id;
}

void Travel::addBooking(Booking *booking) {
    travelBookings.push_back(booking);
}

unsigned int Travel::bookingCount() {
    return travelBookings.size();
}
