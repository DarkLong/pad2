//
//  Customer.cpp
//  Praktikum 1 Xcode
//
//  Created by Tran Giang Long on 13.04.19.
//  Copyright © 2019 Tran Giang Long. All rights reserved.
//

#include "Customer.hpp"

string Customer::getName() const
{
    return name;
}

Customer::Customer(long id) {
    this->id = id;
    this->name = "Unknow";
}

Customer::Customer(Components components) {
    id = stol(components[6]);
    name = components[7];
}

long Customer::getId() {
    return id;
}

void Customer::addTravel(Travel *travel) {
    Travel *checkTravel = findTravel(travel->getId());
    if (checkTravel == nullptr) {
        travelList.push_back(travel);
    }
}

Travel* Customer::findTravel(long id) {
    for (int x = 0; x < travelList.size(); x++) {
        if (travelList[x]->getId() == id) {
            return travelList[x];
        }
    }
    return nullptr;
}

unsigned int Customer::travelCount() {
    return travelList.size();
}
