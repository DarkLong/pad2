#include "checkwindow.h"
#include "ui_checkwindow.h"

CheckWindow::CheckWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CheckWindow)
{
    ui->setupUi(this);
}



CheckWindow::~CheckWindow()
{
    delete ui;
}

void CheckWindow::setTravelAgency(TravelAgency *travelAgency)
{
    this->travekAgency = travelAgency;
    this->ui->tableWidget->setRowCount(travelAgency->allTravel.size());
    this->ui->tableWidget->setColumnCount(5);
    QStringList list;
    list << "id" << "Fehlendes Hotel" << "Ueberflussiges Hotel" << "Ueberflussiger Mietwagen" << "Kein Roundtrip";
    this->ui->tableWidget->setHorizontalHeaderLabels(list);

    for (int x = 0; x < travelAgency->allTravel.size(); x++) {
        this->ui->tableWidget->setItem(x,0, new QTableWidgetItem(QString::number(travelAgency->allTravel[x]->getId())));
        this->ui->tableWidget->setItem(x,1, new QTableWidgetItem(QString::number(travelAgency->allTravel[x]->checkMissingHotel())));
        this->ui->tableWidget->setItem(x,2, new QTableWidgetItem(QString::number(travelAgency->allTravel[x]->checkNeedlessHotel())));
        this->ui->tableWidget->setItem(x,3, new QTableWidgetItem(QString::number(travelAgency->allTravel[x]->checkNeedlessRentalCar())));
        this->ui->tableWidget->setItem(x,4, new QTableWidgetItem(QString::number(travelAgency->allTravel[x]->checkRoundTrip())));
    }
}
