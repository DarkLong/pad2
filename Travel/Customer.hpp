//
//  Customer.hpp
//  Praktikum 1 Xcode
//
//  Created by Tran Giang Long on 13.04.19.
//  Copyright © 2019 Tran Giang Long. All rights reserved.
//

#ifndef Customer_hpp
#define Customer_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include "Travel/Travel.hpp"
#include "libr/Components.h"

using namespace std;

class Customer {
private:
    long id;
    string name;
    vector<Travel*> travelList;
public:
    Customer(long id);
    Customer(Components components);
    void addTravel(Travel *travel);
    long getId();
    
    unsigned int travelCount();
    
    Travel* findTravel(long id);
    string getName() const;

    bool operator<(const Customer &c1) {
        return this->getId() < c1.id;
    }

    bool operator>(const Customer &c1) {
        return this->getId() > c1.id;
    }
};

#endif /* Customer_hpp */
