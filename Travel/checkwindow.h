#ifndef CHECKWINDOW_H
#define CHECKWINDOW_H

#include <QMainWindow>
#include "Booking/TravelAgency.h"

namespace Ui {
class CheckWindow;
}

class CheckWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CheckWindow(QWidget *parent = nullptr);
    ~CheckWindow();
    void setTravelAgency(TravelAgency *travelAgency);

    TravelAgency *travekAgency;
private:
    Ui::CheckWindow *ui;
};

#endif // CHECKWINDOW_H
