//
//  Travel.hpp
//  Praktikum 1 Xcode
//
//  Created by Tran Giang Long on 13.04.19.
//  Copyright © 2019 Tran Giang Long. All rights reserved.
//

#ifndef Travel_hpp
#define Travel_hpp

#include <stdio.h>
#include <vector>
#include "Booking/Booking.h"
#include "libr/Components.h"
#include "libr/graph.h"

using namespace std;

class Travel {
private:
    long id;
    long customerId;
    vector<Booking*> travelBookings;
public:
    Travel(long id, long customerId);
    Travel(Components components);
    void addBooking(Booking *booking);
    long getId();
    
    unsigned int bookingCount();
    long getCustomerId() const;
    void setCustomerId(long value);

    bool checkRoundTrip();
    bool checkMissingHotel();
    bool checkNeedlessHotel();
    bool checkNeedlessRentalCar();

    Graph<Booking*,150> relationship;
};

#endif /* Travel_hpp */
