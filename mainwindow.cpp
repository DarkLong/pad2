#include "mainwindow.h"
#include "newbookingwindow.h"
#include "ui_mainwindow.h"
#include <QAction>
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <Booking/FlightBooking.h>
#include <Booking/HotelBooking.h>
#include "Travel/checkwindow.h"
#include <Booking/RentalCarReservation.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    travelAgency = new TravelAgency();

    ui->setupUi(this);

    // setup menu's actions


    QAction *einlesen_action = new QAction("Einlesen");
    connect(einlesen_action,SIGNAL(triggered()),this, SLOT(open_file()));

    QAction *newBooking = new QAction("Neue Buchung");
    connect(newBooking,SIGNAL(triggered()),this,SLOT(newBooking()));

    QAction *save = new QAction("Save");
    connect(save,SIGNAL(triggered()),this,SLOT(save()));

    QAction *check = new QAction("Check");
    connect(check,SIGNAL(triggered()),this,SLOT(openChecking()));

    QList<QAction*>  actions;
    actions.append(einlesen_action);
    actions.append(save);
    actions.append(newBooking);
    actions.append(check);
    ui->menuBar->addMenu(tr("&File"))->addActions(actions);

    // Event from tableView
    connect(ui->tableView,SIGNAL(doubleClicked(const QModelIndex &)),this,SLOT(onTableClicked(const QModelIndex &)));

    ui->mietwagenContain->setHidden(true);
    ui->flighContainer->setHidden(true);
    ui->HotelContainer->setHidden(true);

    ui->preisNumberField->setMaximum(99999);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::save() {
    QString filePath = QFileDialog::getSaveFileName(this,"Save file", "", ".txt");
    try {
        if (travelAgency->allBookings.size() == 0) {
            QMessageBox::information(this,"Save", "Booking list is empty");
            return;
        }
        travelAgency->save(filePath.toUtf8().constData());
        QMessageBox::information(this,"Save", "Save is successful");
    } catch (FileNotFoundException e) {
        QMessageBox::information(this,"Error", e.what());
    }
}

void MainWindow::openChecking()
{
    CheckWindow *checkWindow = new CheckWindow();
    checkWindow->setTravelAgency(travelAgency);
    checkWindow->show();
}

void MainWindow::open_file() {

    try {
        QString filePath = QFileDialog::getOpenFileName(this,"Open File","/","Text Files (*.txt)");
        qDebug() << filePath;
        travelAgency->reset();
        if (travelAgency->readFile(filePath.toUtf8().constData())) {
            parseData();

            double summe = 0;
            travelAgency->allBookings.reset();
            Booking *booking;
            while (travelAgency->allBookings.isAtEnd()) {
                booking = travelAgency->allBookings.getNode();
                summe += booking->getPrice();
            }

            travelAgency->printCheckTravel();

            QString info = QString("Es wurden %1 Buchungen %2 Kunden und %3 Resien im Gesamtwert von %4 eingelsen")
                    .arg(travelAgency->allBookings.size())
                    .arg(travelAgency->allCustomer.size())
                    .arg(travelAgency->allTravel.size())
                    .arg(summe);

            QMessageBox::information(this,"Einleseergebnis", info);
        } else {
            QMessageBox::information(this,"Error","File is not found!");
        }
    } catch (DataCorruptionException e) {
        travelAgency->reset();
        QString msg = QString::fromUtf8(e.what());
        QMessageBox::information(this,"Exception", msg);
    }


}

void MainWindow::parseData() {
    tableBookingModel = new TableBookingsModel(travelAgency);
    ui->tableView->setModel(tableBookingModel);
}

void MainWindow::onTableClicked(const QModelIndex &index) {
    Booking *booking = travelAgency->allBookings[index.row()];
    ui->buchungsnummer_lineEdit->setText(QString::number(booking->getId()));
    ui->von_calendarWidget->setSelectedDate(QDate::fromString(QString::fromStdString(booking->getFromDate()),"yyyyMMdd"));
    ui->bis_calendarWidget->setSelectedDate(QDate::fromString(QString::fromStdString(booking->getToDate()),"yyyyMMdd"));
    ui->preisNumberField->setValue(booking->getPrice());
    ui->reiseNuumberField->setValue(booking->getTravel_id());

    currentBooking = booking;

    Travel *travel = travelAgency->findTravel(booking->getTravel_id());
    if (travel) {
        ui->kundeNameTextField->setHidden(false);
        Customer *customer = travelAgency->findCustomer(travel->getCustomerId());
        if (customer) {
            ui->kundeNameTextField->setText(QString::fromStdString(customer->getName()));
        }
    } else {
        ui->kundeNameTextField->setHidden(true);
    }

    QString msg = QString::fromStdString(booking->showDetail());
    qDebug() << msg;

    ui->HotelContainer->setHidden(true);
    ui->flighContainer->setHidden(true);
    ui->mietwagenContain->setHidden(true);
    if (FlightBooking *flightBooking = dynamic_cast<FlightBooking*>(booking)) {
        ui->flighContainer->setHidden(false);
        ui->fromTextField->setText(QString::fromStdString(flightBooking->getFromDest()));
        ui->toTextField->setText(QString::fromStdString(flightBooking->getToDest()));
        ui->AirlineTextfield->setText(QString::fromStdString(flightBooking->getAirline()));

//        QString seatStr = "";
//        seatStr += flightBooking->getSeatPref();
        ui->seatTextField->setText(QString::fromStdString(booking->showDetail()));
    } else if (HotelBooking *hotelBooking = dynamic_cast<HotelBooking*>(booking)) {
        ui->HotelContainer->setHidden(false);
        ui->hotelTextField->setText(QString::fromStdString(hotelBooking->getHotel()));
        ui->townTextField->setText(QString::fromStdString(hotelBooking->getTown()));
        ui->smokeCheck->setCheckState(booking->showDetail() == "1" ? Qt::Checked : Qt::Unchecked);
    } else if (RentalCarReservation *carBooking = dynamic_cast<RentalCarReservation*>(booking)) {
        ui->mietwagenContain->setHidden(false);
        ui->abholsstationTextField->setText(QString::fromStdString(carBooking->getPickupLocation()));
        ui->RuckgabesstationTextField->setText(QString::fromStdString(carBooking->getReturnLocation()));
        ui->autovermietungTextField->setText(QString::fromStdString(carBooking->getCompany()));
        ui->VersicherungTextField->setText(QString::fromStdString(booking->showDetail()));
    }
}

void MainWindow::newBooking() {
    NewBookingWindow *newBookingWindow = new NewBookingWindow(this);
    newBookingWindow->travelAgency = travelAgency;
    newBookingWindow->show();
}

void MainWindow::updateData() {
    parseData();
}

